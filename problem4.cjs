// 4. Group items based on the Vitamins that they contain in the following format:
//         {
//             "Vitamin C": ["Orange", "Mango"],
//             "Vitamin K": ["Mango"],
//         }
        
//         and so on for all items and all Vitamins.


let items=require('./3-arrays-vitamins.cjs');

function basedOnVitamins(items){
    return items.reduce((result, item)=>{
        let Vitamin=item.contains.split(', ');
        Vitamin.map((spVitamin)=>{
            if(result[spVitamin]){
                result[spVitamin]=result[spVitamin];
            }
            else{
                result[spVitamin]=[];
            }
            result[spVitamin].push(item.name);
        })
        return result;
    }, {});
}
module.exports=basedOnVitamins;
