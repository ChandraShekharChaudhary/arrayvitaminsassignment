// 3. Get all items containing Vitamin A.

let items=require('./3-arrays-vitamins.cjs');

function itemsContainsVitaminA(items){
    return items.filter(item=> item.contains.includes('Vitamin A'));
}

module.exports=itemsContainsVitaminA;