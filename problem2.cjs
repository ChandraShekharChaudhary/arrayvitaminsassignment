// 2. Get all items containing only Vitamin C.
let items=require('./3-arrays-vitamins.cjs');
function itemsOnlyVitaminC(items){
    return items.filter((item)=> item.contains === 'Vitamin C');
}

module.exports=itemsOnlyVitaminC;