let items=require('./3-arrays-vitamins.cjs');
const getAvailableItems=require('./problem1.cjs');
const itemsOnlyVitaminC=require('./problem2.cjs');
const itemsContainsVitaminA=require('./problem3.cjs');
const basedOnVitamins=require('./problem4.cjs');
const sortByNumberOfVitamins=require('./problem5.cjs');

// console.log(getAvailableItems(items));
// console.log(itemsOnlyVitaminC(items));
// console.log(itemsContainsVitaminA(items));
// console.log(basedOnVitamins(items));
console.log(sortByNumberOfVitamins(items));