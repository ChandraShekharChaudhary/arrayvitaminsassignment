// 5. Sort items based on number of Vitamins they contain.
// let items=require('./3-arrays-vitamins.cjs');

function sortByNumberOfVitamins(items){
   return items.sort((first, second)=>{
        let firstLength=first.contains.split(',').length;
        let secondLength=second.contains.split(',').length;

        if(firstLength>secondLength){
            return 1;
        }else if(firstLength<secondLength){
            return -1;
        }else{
            return 0;
        }
    })
}

module.exports=sortByNumberOfVitamins;